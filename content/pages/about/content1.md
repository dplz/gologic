---
title: 'Tecnologia'
date: 2018-12-06T09:29:16+10:00
weight: 1
background: 'images/imgbluetooth.png'
align: right
---

Grazie alla tecnologia Bluetooth/BLE, l'applicazione può connettersi ai tuoi dispositivi BLE e inviare comandi che automatizzano processi senza l'ausilio di una connessione ad internet o telefonica. Il tuo telecomando sempre a disposizione nel tuo Smartphone.
