---
title: 'Quando usare GoLogic App?'
weight: 3
date: 2018-12-06T09:29:16+10:00
background: 'images/gologicboard.png'
align: right
#button: 'Contact Us'
#buttonLink: 'contact'
---

L'applicazione può comunicare con i dispositivi BLE in qualunque luogo tu voglia installarli.Quando sei dentro o fuori casa, nei luoghi di lavoro...non ci sono limiti!