---
title: 'App'
date: 2018-12-06T09:29:16+10:00
layout: 'about'
heroHeading: 'Cosa è GoLogic?'
heroSubHeading: "L'applicazione che consente di comandare i tuoi dispositivi BLE."
heroBackground: 'images/man-opening-garage-with-phone.jpg'
---

<div>
{{< content-strip-left "/pages/about" "content1" >}}
</div>
<div>
{{< content-strip-right "/pages/about" "content2" >}}
</div>
<div>
{{< content-strip-center "/pages/about" "content3" >}}
</div>
