---
title: 'Istruzioni'
date: 2018-02-22T17:01:34+07:00
---

Di seguito il link per scaricare il file contenente le istruzioni per l'utilizzo dell'app GoLogic con i dispositivi supportati GoLogic:

[Istruzioni GoLogic App](/istruzioni/gologic.pdf/)
